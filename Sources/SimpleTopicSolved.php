<?php declare(strict_types=1);

/**
 * Add the button to the topic buttons.
 *
 * hook="integrate_display_buttons"
 */
function simpleTopicSolvedAddButton(&$buttons): void {
    global $context, $scripturl, $modSettings, $board, $user_info;

    simpleTopicSolvedLanguage();

    // Solving button
    if (simpleTopicSolvedAllowedTo((int) $context['topicinfo']['id_member_started'])) {
        $is_solved = str_starts_with($context['topicinfo']['subject'], '[SOLVED]');

        $buttons['solve'] = [
            'text' => 'SimpleTopicSolved_mark_'.($is_solved ? 'not_' : '').'solved',
            'url' => $scripturl . '?action=simpletopicsolved;topic=' . $context['current_topic'] . ';' . $context['session_var'] . '=' . $context['session_id']
        ];
    }
}

/**
 * Add the action to mark a topic as solved or unsolved.
 *
 * hook="integrate_actions"
 */
function simpleTopicSolvedActions(&$actions): void {
    $actions['simpletopicsolved'] = ['SimpleTopicSolved.php', 'simpleTopicSolvedSolve'];
}

/**
 * Read from the database the subject of the first message in the topic,
 * add or remove [SOLVED] at the beginning, and write the subject back to the database.
 *
 * actions['simpletopicsolved']
 */
function simpleTopicSolvedSolve(): void {
    global $smcFunc, $user_info;

    simpleTopicSolvedLanguage();

    if (!isset($_REQUEST['topic']) || empty($_REQUEST['topic'])) {
        fatal_lang_error('TopicSolved_error_no_topic', false);
    }

    checkSession('get');

    $request = $smcFunc['db_query']('', '
        SELECT t.id_topic, m.subject, t.id_first_msg, m.id_member
        FROM {db_prefix}topics AS t
            LEFT JOIN {db_prefix}messages AS m ON (m.id_msg = t.id_first_msg)
        WHERE t.id_topic = {int:topic}',
        [
            'topic' => (int) $_REQUEST['topic'],
        ]
    );
    $topic_info = $smcFunc['db_fetch_assoc']($request);
    $smcFunc['db_free_result']($request);

    if (empty($topic_info)) {
        fatal_lang_error('TopicSolved_error_no_topic', false);
    }

    // Do you have permission to solve this topic?
    if (!simpleTopicSolvedAllowedTo((int) $topic_info['id_member'])) {
        redirectexit('topic=' . $_REQUEST['topic'] . '.0');
        return;
    }

    $subject = $topic_info['subject'];
    if (str_starts_with($topic_info['subject'], '[SOLVED]')) {
        $subject = trim(substr($topic_info['subject'], 8));
    } else {
        $subject = '[SOLVED] '.$subject;
    }

    $smcFunc['db_query']('', '
        UPDATE {db_prefix}messages
        SET subject = {string:subject}
        WHERE id_msg = {int:message_id}',
        [
            'subject' => $subject,
            'message_id' => $topic_info['id_first_msg'],
        ]
    );

    // We are done here
    redirectexit('topic=' . $_REQUEST['topic'] . '.0');
}

/**
 * The creator of the topic, the admininstrators and the moderators can mark the topic as solved.
 *
 * @param  $topic_user_id the user having created the topic.
 */
function simpleTopicSolvedAllowedTo($topic_user_id): bool {
    global $user_info;
    if ($user_info['id'] ===  $topic_user_id) {
        return true;
    }
    if ($user_info['is_admin'] === true || $user_info['is_mod'] === true) {
        return true;
    }
    return false;
}

/**
 * Let smf load the mod's language file.
 */
function simpleTopicSolvedLanguage() : void {
    loadLanguage('SimpleTopicSolved');
}
