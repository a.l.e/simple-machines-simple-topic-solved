# Simple Solved Topics

Add a button to the topic, for updating the subject with "[SOLVED]".

![Screenshot](SimpleTopicSolved.gif)

This mod can be useful if the forums limits how long the users can edit their posts, but you want to let them mark the topic as solved.

This is a much simplified version of the [Topic Solved](https://custom.simplemachines.org/index.php?mod=4339) mod:

- It does not have any settings.
- The installation does not modify the database.
- The button simply adds the string "[SOLVED]" at the beginning of the subject by updating the database entry.

## Details

- The user having started the topic, the moderators and the admins see the "Mark Solved" button at the start of each topic.
- Clicking the button adds "[SOLVED]" at the beginning of the topic's subject.
- The button changes to "Clear Solved"
